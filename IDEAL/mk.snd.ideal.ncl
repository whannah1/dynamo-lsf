load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_LSF.ncl"
procedure smooth (V[*],num)
begin
  sz = dimsizes(V)
  do i = 0,num-1
    tmp = V
  do k = 1,sz(0)-2
    V(k) = sum( (/tmp(k-1),tmp(k),tmp(k+1)/)*(/0.25,0.5,0.25/) )
  end do
  end do
end
begin

	debug 		= True

	sa = "ssa"

	ofile1 = "~/Research/DYNAMO/LSF/case_files_"+sa+"_IDEAL/snd.sa_shear"
	ofile2 = "~/Research/DYNAMO/LSF/case_files_"+sa+"_IDEAL/snd.no_shear"
	  
;======================================================================================
;======================================================================================
	time = LoadFieldsTime(sa)
	
	p   = LoadFields("p"    ,sa)
	z   = LoadFields("z"    ,sa)
	Tp  = LoadFields("theta",sa)
	T   = LoadFields("T"    ,sa)
	wmr = LoadFields("wmr"  ,sa)/1000.
	u   = LoadFields("u"    ,sa)
	v   = LoadFields("v"    ,sa)
	w   = LoadFields("omega",sa)
	vor = LoadFields("vort" ,sa)
	div = LoadFields("div"  ,sa)
	
	uns = conform(u,dim_avg_n(u,1),0)
	vns = conform(v,dim_avg_n(v,1),0)
	
	if True then
	  q1  = LoadQ("q1"   ,sa)
	  q2  = LoadQ("q2"   ,sa)
	else
	  qvar = "Q1_dse"
	  ifile = "~/Research/DYNAMO/LSF/"+sa+"."+qvar+".nc"
	  infile = addfile(ifile,"r")
	  q1 = infile->$qvar$(:,:,0,0)
	  ifile = "~/Research/DYNAMO/LSF/"+sa+".Q2.nc"
	  infile = addfile(ifile,"r")
	  q2 = infile->Q2(:,:,0,0)
	  q1 = (/q1*24.*3600./cpd/)
	  q2 = (/q2*24.*3600./cpd/)
	end if
	
	q1 = (/-q1/)
	
	q = wmr/(1.+wmr) 
	
	ps = LoadFieldsSfc("p",sa)
	
	avg_z   = dim_avg_n_Wrap(z,0)
	avg_p   = dim_avg_n_Wrap(p,0)
	avg_T   = dim_avg_n_Wrap(T,0)
	avg_Tp  = dim_avg_n_Wrap(Tp,0)
	avg_q   = dim_avg_n_Wrap(q,0)
	avg_s   = dim_avg_n_Wrap( (T*cpd+z*g)/cpd ,0)
	avg_h   = dim_avg_n_Wrap( (T*cpd+z*g+Lv*q)/cpd ,0)
	avg_u   = dim_avg_n_Wrap(u,0)
	avg_v   = dim_avg_n_Wrap(v,0)
	avg_uns = dim_avg_n_Wrap(uns,0)
	avg_vns = dim_avg_n_Wrap(vns,0)
	avg_w   = dim_avg_n_Wrap(w,0)
	avg_div = dim_avg_n_Wrap(div,0)
	avg_vor = dim_avg_n_Wrap(vor,0)
	avg_q1  = dim_avg_n_Wrap(q1,0)
	avg_q2  = dim_avg_n_Wrap(q2,0)
	avg_ps  = dim_avg_n_Wrap(ps,0)
	
	avg_q = avg_q*1000.
	
  if True then
    smooth(avg_q1, 8)
    smooth(avg_q2, 8)
    smooth(avg_u , 4)
    smooth(avg_v , 4)
  end if

;======================================================================================
;======================================================================================
if True then
  fig_type = "x11"
  ;fig_file = "~/Research/DYNAMO/LSF/IDEAL/plot.snd."+sa+".IDEAL.sa_shear"
  fig_file = "~/DYNAMO/LSF/IDEAL/plot.snd."+sa+".IDEAL.sa_shear"
  wks = gsn_open_wks(fig_type,fig_file)
  plot = new(4,graphic)
  	res = True
  	res@gsnDraw  = False
  	res@gsnFrame = False
  qlev = q1&lev
  lev = avg_z
  ;lev = avg_p
  ;res@trYReverse = True

  	res@gsnLeftString = "MSE, DSE"
  plot(0) = gsn_csm_xy(wks,(/avg_h,avg_s/),lev,res)
  	res@gsnLeftString = "Omega"
  plot(1) = gsn_csm_xy(wks,(/avg_w,lev*0./),lev,res)
  	res@gsnLeftString = "U, V"
  plot(2) = gsn_csm_xy(wks,(/avg_u,avg_v,lev*0./),lev,res)
  	res@gsnLeftString = "LS T tend., LS Q tend."
  plot(3) = gsn_csm_xy(wks,(/avg_q1,avg_q2,qlev*0./),qlev,res)
  	pres = True
  	pres@gsnPanelXWhiteSpacePercent = 10.
  	pres@gsnPanelYWhiteSpacePercent = 10.
  gsn_panel(wks,plot,(/2,2/),pres)
  print("")
  print("  "+fig_file+"."+fig_type)
  print("")  
end if
if debug then
  exit
end if
;======================================================================================
; write out to an ascii file
;======================================================================================
  t1 = 0
  num_t = 2
  num_z = dimsizes(p(0,:))
  num_r = num_z+1
  str = new((num_t-t1)*num_r+1,string)
  str(0) = " z[m] p[mb] tp[K] q[g/kg] u[m/s] v[m/s]"
  
  itime = time
  delete(time)
  time = (/320.,999./)
;======================================================================================
;======================================================================================
  do t = 0,1
    data = "  "+sprintf("%8.2f",avg_z)  +"    "+ sprintf("%8.2f",avg_p)  +"    "+ \
         	sprintf("%8.2f",avg_Tp) +"    "+ sprintf("%8.2f",avg_q)  +"    "+ \
         	sprintf("%8.2f",avg_u)  +"    "+ sprintf("%8.2f",avg_v)
    header = " "+time(t)+",   "+num_z+",  "+avg_ps+"  day,levels,pres0"
    tt = 1+(t-t1)*num_r
    str(tt             ) = header
    str(tt+1:tt+num_r-1) = data
      delete(data)
  end do
  
  print(str(:num_r-1))
  asciiwrite(ofile1,str)
;======================================================================================
; No shear version
;======================================================================================
  do t = 0,1
    data = "  "+sprintf("%8.2f",avg_z)  +"    "+ sprintf("%8.2f",avg_p)  +"    "+ \
         	sprintf("%8.2f",avg_Tp)  +"    "+ sprintf("%8.2f",avg_q)  +"    "+ \
         	sprintf("%8.2f",avg_uns)+"    "+ sprintf("%8.2f",avg_vns)
    header = " "+time(t)+",   "+num_z+",  "+avg_ps+"  day,levels,pres0"
    tt = 1+(t-t1)*num_r
    str(tt             ) = header
    str(tt+1:tt+num_r-1) = data
      delete(data)
  end do
  
  print(str(:num_r-1))	
  asciiwrite(ofile2,str)
;======================================================================================
;======================================================================================
  	print("")
  	print("  "+ofile1)
  	print("  "+ofile2)
  	print("")
end
